import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:math';

class _ColorCustom extends StatefulWidget {
  @override
  State<_ColorCustom> createState() => _ColorState();
}

class _ColorState extends State<_ColorCustom>{
  Color color;

  @override
  void initState() {
    super.initState();

    color = Colors.white;
  }
  @override
  Widget build(BuildContext context){

    return Center(
      child: GestureDetector(
        onTap: () {
             setState(() {
               color = Color((Random().nextDouble() * 0xFFFFFF).toInt() << 0).withOpacity(1.0);;
             });
        },
        child: Container(
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
              color: color,
          ),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Center(
            child: Text('Hey There',
            style: TextStyle(fontSize: 30.0),
            ),
          ),
        )
      ),
    );
  }
}

void main(){
  runApp(
      new MaterialApp(
        debugShowCheckedModeBanner: false,
           home: new Scaffold(
               body: Center(
                 child: new _ColorCustom(),
               )

            ),
      ),
  );
}